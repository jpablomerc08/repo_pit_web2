
export class CasosModel {

  id     : string;
  nombre : string;
  poder  : string;
  vivo   : boolean;

  constructor() {
    this.vivo = true;
  }

}

export class Filtro {

  Nombre : string;
  NroDocumentoIdentidad : string;
  EstadoId : number;
}

export class InfoCaso {
  nuevos: number;
  confirmados: number;
  recuperados: number;
  fallecidos: number;
}

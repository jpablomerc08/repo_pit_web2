import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CasosModel } from '../models/casos.model';
import { map, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  // private URL_WS = 'http://edinsonaldaz-001-site1.htempurl.com/api/';
  constructor( private http: HttpClient ) { }

  getPais(): Observable<any> {
    return this.http.get<any>(`/api/Paises`);
  }

  getDepartamento( id: string) {
    return this.http.get('/api/Departamentos');
  }

  getProvincia( id: string) {
    return this.http.get(`/api/Provincias/${ id }`);
  }

  getDistrito( id: string) {

    return this.http.get(`/api/Distritos/${ id }`);
  }

  getTipoDocumento() {
    return this.http.get('/api/TiposDocumentoIdentidad');
  }

  getEstados() {
    return this.http.get('/api/Estados');
  }

  filtro( filtro: any ) {
    return this.http.post('/api/Ciudadanos/filtrar', filtro);
  }

  info( filtro: any ) {
    return this.http.post('/api/Estadisticas/Filtrar', filtro);
  }

  getCasoDetalles( id: string ) {
    return this.http.get(`/api/Ciudadanos/${ id }/detail`);
  }





}

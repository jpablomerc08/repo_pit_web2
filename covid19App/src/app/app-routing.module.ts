import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoComponent } from './pages/info/info.component';
import { RegistrarCasosComponent } from './pages/registrar-casos/registrar-casos.component';
import { CasosActivosComponent } from './pages/casos-activos/casos-activos.component';
import { UsuarioComponent } from './pages/usuario/usuario.component';


const routes: Routes = [
  { path: 'info', component: InfoComponent },
  { path: 'casos', component: CasosActivosComponent },
  { path: 'registro/:id', component: RegistrarCasosComponent },
  { path: 'usuario', component: UsuarioComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'info' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ValidadoresService } from 'src/app/services/validadores.service';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-registrar-casos',
  templateUrl: './registrar-casos.component.html',
  styleUrls: ['./registrar-casos.component.css']
})
export class RegistrarCasosComponent implements OnInit {

  forma: FormGroup;
  classHidden = 'd-none';
  div: number;
  date = new Date();
  today: string;

  // Combos
  paises: any[] = [];
  estados: any[] = [];
  tipoDoc: any[] = [];
  departamento: any[] = [];
  provincia: any[] = [];
  distrito: any[] = [];

  cboDepartamento: string;
  cboProvincia: string;
  cboDistrito: string;

  // Partes del Fortmulario
  partFrom: any = [{
    div    : 0,
    activo : false,
    title  : 'Datos Personales'
  }, {
    div    : 1,
    activo : true,
    title  : 'Dirección'
  }, {
    div    : 2,
    activo : true,
    title  : 'Contacto'
  }, {
    div    : 3,
    activo : true,
    title  : 'Sintomas'
  }, {
    div    : 4,
    activo : true,
    title  : 'Situaciones de riesgo'
  }];

  constructor( private fb: FormBuilder,
               private router: ActivatedRoute,
               private validadores: ValidadoresService,
               private ubigeoService: UbigeoService) {
    this.crearFormulario();
    this.cargarDataAlFormulario();
    this.cargarListeners();


   }

  ngOnInit(): void {

    const id = this.router.snapshot.paramMap.get('id');
    console.log(id);

    if (id !== 'nuevo') {
      this.ubigeoService.getCasoDetalles( id )
        .subscribe( resp => {
          console.log(resp);
        });
      // this.heroesService.getHeroe( id )
      //   .subscribe( (resp: HeroeModel) => {
      //     this.heroe = resp;
      //     this.heroe.id = id;
      //     console.log(resp);
      //   });
    }

    this.cargaCombos();
    this.today = new Date().toISOString().split('T')[0];

  }

  // Nuevo o Update






  cargarListeners() {
    this.departamentoListeners();
    this.provinciaListeners();
    this.distritoListeners();
  }

  cargaCombos() {
    this.ubigeoService.getPais()
      .subscribe( (paises: any) => {
        this.paises = paises;
        this.paises.unshift({
          codigoISO: '',
          nombre: '[ Seleccione Pais ]',
          paisId: 0
        });
      });

    this.ubigeoService.getEstados()
      .subscribe( (resp: any) => {
        this.estados = resp.filter( res => res.estadoid > 2);
        this.estados.unshift({
          estadoid: 0,
          nombre: '[ Seleccione Estado ]',
          descripcion: ''
        });

      });

    this.ubigeoService.getTipoDocumento()
      .subscribe( (resp: any) => {
        this.tipoDoc = resp;
        this.tipoDoc.unshift({
          tipoDocumentoIdentidadId: 0,
          descripcionLarga: '[ Seleccione Documento ]',
          descripcionCorta: ''
        });
      });

    this.ubigeoService.getDepartamento('')
      .subscribe( (resp: any) => {

        this.departamento = resp;
        this.departamento.unshift({
          departamentoId: 0,
          ubigeo: '',
          nombre: '[ Seleccione Departamento ]'
        });
      });



    // this.ubigeoService.getProvincia(this.cboDepartamento)
    //   .subscribe( (resp: any) => {
    //     this.provincia = resp;
    //     this.provincia.unshift({
    //       provinciaId: 0,
    //       ubigeo: '',
    //       nombre: '[ Seleccione Provincia ]'
    //     });
    //   });



    // this.ubigeoService.getDistrito('1')
    //   .subscribe( (resp: any) => {
    //     console.log(resp);
    //     this.distrito = resp;
    //     this.distrito.unshift({
    //       distritoId: 0,
    //       ubigeo: '',
    //       nombre: '[ Seleccione Distrito ]'
    //     });
    //   });
  }


  get pasatiempos() {
    return this.forma.get('pasatiempos') as FormArray;
  }

  // Validaciones

  validarCampo( campo: string ) {
    return this.forma.get(campo).invalid && this.forma.get(campo).touched;
  }

  // get pass2NoValido() {
  //   const pass1 = this.forma.get('pass1').value;
  //   const pass2 = this.forma.get('pass2').value;
  //   return ( pass1 === pass2 ) ? false : true ;
  // }

  crearFormulario() {
    this.forma = this.fb.group({
      // Primer Div
      Ciudadano: this.fb.group({
        CiudadanoId              : [''],
        Nombre                   : [''],
        ApellidoPaterno          : [''],
        ApellidoMaterno          : [''],
        Sexo                     : [''],  // Boleean
        NacionalidadId           : [''],  // Entero
        TipoDocumentoIdentidadId : [''],  // Entero
        NroDocumentoIdentidad    : [''],
        Celular                  : [''],
        Correo                   : [''],
        FechaNacimiento          : [''],
        EstadoId                 : ['']   // Entero
      }),
      // Segundo Div
      Contactos: this.fb.group({
        contactoId               : [''], // Entero
        ciudadanoId              : [''], // Entero
        nombreContacto           : [''],
        parentesco               : [''],
        telefono                 : [''],
        correo                   : ['']
      }),
      Direcciones: this.fb.group({
        DireccionId              : ['']
      }),


      // Primer Div
      pais     : [ '', [ Validators.required ] ],
      tipDoc   : [ '', [ Validators.required ] ],
      nroDoc   : [ '', [ Validators.required, Validators.min(0), Validators.max(9999999) ] ],
      fecNac   : [ '', [ Validators.required ] ],
      sexo     : [ '', [ Validators.required ] ],
      telefono : [ '', [ Validators.required ] ],
      correo   : [ '', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$') ] ],
      estado   : [ '', [ Validators.required ] ],

      // Segundo Div
      depDir   : [ '', [ Validators.required ] ],
      proDir   : [ '', [ Validators.required ] ],
      disDir   : [ '', [ Validators.required ] ],
      desDir   : [ '', [ Validators.required ] ],

      // Tercer Div
      nomCont  : [ '', [ Validators.required ] ],
      parentesco: [ '', [ Validators.required ] ],
      telefonoCont: [ '', [ Validators.required ] ],
      CorreoCont: [ '', [ Validators.required ]],

      // Cuarto Div
      chkP01   : [ '', [ Validators.required ]],
      chkP02   : [ '', [ Validators.required ]],
      chkP03   : [ '', [ Validators.required ]],
      chkP04   : [ '', [ Validators.required ]],
      chkP05   : [ '', [ Validators.required ]],

      // Quinto Div



    //   // no va
    //   nombre   : [ '', [ Validators.required, Validators.minLength(5) ] ],
    //   apellido : [ '', [ Validators.required, Validators.minLength(5), this.validadores.noHerrera ] ],
    //   correo1   : [ '', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$') ] ],
    //   usuario  : [ '', , this.validadores.existeUsuario ],
    //   pass1    : [ '', [ Validators.required ]],
    //   startDate : [ '' ],
    //   pass2    : [ '', [ Validators.required ]],
    //   direccion: this.fb.group({
    //     distrito : [ '', [Validators.required] ],
    //     ciudad   : [ '', [Validators.required] ]
    //   }),
    //   pasatiempos: this.fb.array([
    //   ])
    // }, {
    //   validators: [ this.validadores.passwordsIguales('pass1', 'pass2') ]
    });
  }

  cargarDataAlFormulario() {

    // this.forma.setValue({
    this.forma.reset({
      pais: 177,
      tipDoc: 0,
      nroDoc: '',
      fecNac: '',
      estado: 0,
      depDir: 0,
      proDir: 0,
      disDir: 0
    });

  }

  departamentoListeners() {
    this.forma.get('depDir').valueChanges.subscribe( valor => {
      this.cboDepartamento = valor;
      this.ubigeoService.getProvincia(this.cboDepartamento)
      .subscribe( (resp: any) => {
        this.provincia = resp;
      });
    });
  }

  provinciaListeners() {
    this.forma.get('proDir').valueChanges.subscribe( valor => {
      this.cboProvincia = valor;
      this.ubigeoService.getDistrito(this.cboProvincia)
      .subscribe( (resp: any) => {
        console.log(resp);
        this.distrito = resp;
      });
    });
  }

  distritoListeners() {
    this.forma.get('disDir').valueChanges.subscribe( valor => {
      this.cboDistrito = valor;
    });
  }


  guardar() {

    console.log(this.forma);

    if ( this.forma.invalid ) {

      return Object.values( this.forma.controls ).forEach( control => {
        if ( control instanceof FormGroup ) {
          Object.values( control.controls ).forEach( ctrl => ctrl.markAsTouched());
        } else {
          control.markAsTouched();
        }
      });
    }

    // Posteo de información
    this.forma.reset();
  }


  // Prueba de Navegacion
  pageDiv( div: number) {

    this.partFrom[div].activo = false;

    this.partFrom.forEach( ctrl => {
      if (!ctrl.activo) {
        console.log(ctrl.div);
        if (ctrl.div !== div) {
          console.log(ctrl.div);
          this.partFrom[ctrl.div].activo = true;
        }
      }
    });
  }

}

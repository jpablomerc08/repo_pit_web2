import { Component, OnInit } from '@angular/core';
import { UbigeoService } from 'src/app/services/ubigeo.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { InfoCaso } from 'src/app/models/casos.model';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})


export class InfoComponent implements OnInit {

  today: string;
  forma: FormGroup;
  cargando = true;

  departamento: any[] = [];
  provincia: any[] = [];
  distrito: any[] = [];
  casosResp: InfoCaso;

  cboDepartamento: string;
  cboProvincia: string;
  cboDistrito: string;



  constructor( private ubigeoService: UbigeoService,
               private fb: FormBuilder) {
                this.cargarDatos();
                this.crearFormulario();
                this.cargarDataAlFormulario();
                this.cambios();
                }

  ngOnInit(): void {
    this.cargaDepartamento();
    this.cargarListeners();
  }

  cargarDatos() {
    this.today = new Date().toISOString().split('T')[0];
    console.log(this.today);
  }

  cargaDepartamento() {
    this.ubigeoService.getDepartamento('')
      .subscribe( (resp: any) => {

        this.departamento = resp;
        this.departamento.unshift({
          departamentoId: 0,
          ubigeo: '',
          nombre: '[ Seleccione Departamento ]'
        });
      });

  }

  cargarListeners() {
    this.departamentoListeners();
    this.provinciaListeners();
    this.distritoListeners();
  }

  departamentoListeners() {
    this.forma.get('DepartamentoId').valueChanges.subscribe( valor => {
      this.cboDepartamento = valor;
      this.ubigeoService.getProvincia(this.cboDepartamento)
      .subscribe( (resp: any) => {
        this.provincia = resp;
        this.cambios();
      });
    });
  }

  provinciaListeners() {
    this.forma.get('ProvinciaId').valueChanges.subscribe( valor => {
      this.cboProvincia = valor;
      this.ubigeoService.getDistrito(this.cboProvincia)
      .subscribe( (resp: any) => {
        console.log(resp);
        this.distrito = resp;
        this.cambios();
      });
    });
  }

  distritoListeners() {
    this.forma.get('DistritoId').valueChanges.subscribe( valor => {
      this.cboDistrito = valor;
      this.cambios();
    });
  }

  crearFormulario() {
    this.forma = this.fb.group({
      Fecha: [''],
      DepartamentoId: [''],
      ProvinciaId: [''],
      DistritoId: ['']
    });
  }

  cargarDataAlFormulario() {

    // this.forma.setValue({
    this.forma.reset({
      Fecha: this.today,
      DepartamentoId: '0',
      ProvinciaId: '0',
      DistritoId: '0'
    });

  }

  cambios() {

    if (this.forma.value.DepartamentoId === '0') {
      this.forma.value.DepartamentoId = null;
    } else {
      // tslint:disable-next-line:radix
      this.forma.value.DepartamentoId = parseInt( this.forma.value.DepartamentoId );
    }

    if (this.forma.value.ProvinciaId === '0') {
      this.forma.value.ProvinciaId = null;
    } else {
      // tslint:disable-next-line:radix
      this.forma.value.ProvinciaId = parseInt( this.forma.value.ProvinciaId );
    }


    if (this.forma.value.DistritoId === '0') {
      this.forma.value.DistritoId = null;
    } else {
      // tslint:disable-next-line:radix
      this.forma.value.DistritoId = parseInt( this.forma.value.DistritoId );
    }

    this.ubigeoService.info(this.forma.value).subscribe( (resp: InfoCaso) => {
      this.casosResp = resp;
      this.cargando = false;
    });
  }




}
